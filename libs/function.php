<?php

function get_hdd() {
    $output = shell_exec('df -h');
    return '<pre style="color: darkblue;">'.$output.'</pre>';
}

function get_memory() {
    $output = shell_exec('free --mega');
    return '<pre style="color: darkblue;">'.$output.'</pre>';
}

function email_template_html($data) {
    $template = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>VPS Monitoring</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        <body>
                <pre>Hi, berikut adalah hasil pantauan dari server : '.$data->server_name.' <br/>per tanggal '.date("d-m-Y").'</pre>
                <pre style="margin-top: 50px; color: darkgreen;">Data Monitoring Hardisk : </pre>
                '.get_hdd().'

                <pre style="margin-top: 50px; color: darkgreen;">Data Monitoring Memory : </pre>
                '.get_memory().'

                <pre style="margin-top: 50px;">Untuk update saat ini bisa dibuka di panel vps anda di '.$data->server_location.'.</pre>

                <pre style="margin-top: 100px;">Regards,</pre>

                <pre style="margin-top: 50px;">Admin BOT</pre>
        </body>
    </html>';

    return $template;
}


?>