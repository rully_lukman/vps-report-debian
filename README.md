# VPS Report Debian To Email

### Installation

1. Clone to your web directory
2. Run composer install on this project root directory
3. Copy mail-default.php to mail.php, insert smtp credentials on mail.php like
    ```
    $mail->Host       = 'smtp.example.com';                    
    $mail->SMTPAuth   = true;                                   
    $mail->Username   = 'yoursmtpusername';                     
    $mail->Password   = 'yoursmtppassword';      
    ```
4. Copy config-default.php to config.php, insert all credentials data such as $email_sender , $email_target, server_name and server_location
5. Create scheduler php execute for mail.php
6. Run in your cron execute it whatever you want in : 
    ```
    $YOUR_FILE_PATH/vps-report-debian/mail.php >/var/log/syslog 2>&1
    ```
Notes : don't forget to do the > /var/log things to discard unnecessary report on local machine

### Changelog version 1.0.0

- Add base configuration to config.php
- File execute in mail.php
- Using phpmailer library, see : https://github.com/PHPMailer/PHPMailer