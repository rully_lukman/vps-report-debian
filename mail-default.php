<?php
    //COPY AND RENAME TO mail.php

    // Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    // Load Composer's autoloader
    require 'vendor/autoload.php';
    include "libs/function.php";
    include "config.php";

    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    foreach($email_target as $d) {

        
        try {
            //Server settings
            $mail->SMTPDebug = 0;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = 'smtp.example.com';                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'yoursmtpusername';                     // SMTP username
            $mail->Password   = 'yoursmtppassword';                               // SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($email_sender['email'], $email_sender['name']);
            $mail->addAddress($email_target[0], $email_target[1]);     // Add a recipient
            $mail->addReplyTo($email_sender['email'], $email_sender['name']);
            $mail->addCC($cc);

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'VPS Monitoring on '.date("d-m-Y");
            $mail->Body    = email_template_html($data);

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

?>
